STAGE 1 // PENGUMUMAN & BIO
Para Guru diharapkan BISA UNTUK LIHAT PENGUMUMAN dan UBAH BIODATA secara mandiri, dengan cara :
1. Di halaman dashboard lihat Pengumuman yang berjudul HUT RI 76, lalu Klik tombol Lihat Detail (selesai)
2. Masih di halaman dashboard, sekarang lihat dan klik menu Biodata
3. Lalu klik tombol Ubah Biodata
4. Lalu klik tombol Simpan data (selesai)

STAGE 2 // ABSENSI
Para Guru diharapkan BISA UNTUK LIHAT ABSENSI PELAJAR secara mandiri, dengan cara :
1. Lihat menu dengan nama Absensi, lalu klik menu Absensi
2. Lalu klik option pilih tahun pada Tahun Ajaran, dan pilih 2020/2021 Genap
3. Sekarang pilih Tingkat X dan pilih Kelas X IPS 1 (selesai)

STAGE 3 // UAS
Para Guru diharapkan BISA UNTUK LIHAT DAN EDIT NILAI UAS secara mandiri, dengan cara
1. Di halaman dashboard, lihat dan klik menu Nilai 
2. Pilih menu Nilai UAS
3. Masuk halaman UAS, lalu klik option pilih tahun pada Tahun Ajaran, dan pilih 2020/2021 Genap, sekarang pilih Tingkat X dan pilih Kelas X IPS 1
4. Lihat siswa dengan nama Abdul Minasika lalu klik edit nilai
5. Jika sudah maka klik Ubah Data (selesai)

STAGE 4 // NILAI UTS
Para Guru diharapkan BISA UNTUK LIHAT DAN EDIT NILAI UTS secara mandiri, dengan cara
1. Di halaman dashboard, lihat dan klik menu Nilai 
2. Pilih menu Nilai UTS
3. Masuk halaman UTS, lalu klik option pilih tahun pada Tahun Ajaran, dan pilih 2020/2021 Genap, sekarang pilih Tingkat X dan pilih Kelas X IPS 1
4. Lihat siswa dengan nama Abdul Minasika lalu klik edit nilai
5. Jika sudah maka klik Ubah Data (selesai)

STAGE 5 // JADWAL & SPP
Para Guru diharapkan BISA UNTUK LIHAT SPP dan DOWNLOAD JADWAL secara mandiri, dengan cara :
1. Lihat menu dengan nama Jadwal Mengajar, lalu klik menu Jadwal Mengajar
2. Lalu Klik tombol download jadwal pada kelas apapun, dan klik notifikasi (selesai)
3. Masih di halaman Jadwal, sekarang lihat menu Status SPP, lalu klik menu Status SPP
4. Lalu klik option pilih tahun pada Tahun Ajaran, dan pilih 2020/2021 Genap
5. Sekarang pilih Tingkat X dan pilih Kelas X IPS 1 (selesai)